/**
 *\file Reference.h
 *\brief Header de la classe Reference
 *\author Toma Gagne
 *\version 1
 *\date 22 fevrier 2019
 */

#ifndef REFERENCE_H_
#define REFERENCE_H_

#include <iostream>
#include <string>

namespace biblio
{
	class Reference;
}

/**
 * \class Reference
 * \brief Classe implantant le concept de references bibliographiques
 * 		Attributs: 	int m_annee: annee d'edition de la reference
 * 					string m_auteurs: nom de l'auteur ou des auteurs de la reference
 * 					string m_identifiant: l'identifiant de la reference. Peut etre un code ISSN ou ISBN
 * 					string m_titre: titre de la reference
 */
class biblio::Reference
{
public:
	Reference(std::string,std::string, int, std::string);
	int reqAnnee() const;
	std::string reqAuteurs() const;
	std::string reqIdentifiant() const;
	std::string reqTitre() const;
	std::string reqReferenceFormate();
	void asgAuteurs(const std::string& p_auteurs);
	bool operator==(Reference autreReference) const;

private:
	int m_annee;
	std::string m_auteurs;
	std::string m_identifiant;
	std::string m_titre;
};


#endif /* REFERENCE_H_ */
