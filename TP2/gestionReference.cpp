/**
 *\file testClasseReference.cpp
 *\brief Programme interactif pour la création d'une "Référence"
 *\author Toma Gagne
 *\version 0.1
 *\date 22 fevrier 2019
 *
 * Programme interactif minimaliste pour obtenir interactivement avec l'usager les données
 * nécessaires pour créer une "Référence", l'éditer l'écran, proposer de modifier les
 * auteurs, l'éditer de nouveau pour pouvoir constater que la modification est effective.
 */

#include "validationFormat.h"
#include "Reference.h"

using namespace std;

/**
 * \fn int main()
 * \brief Entrée du programme
 *
 * \return 0 = Arrêt normal du programme.
 */
int main()
{
	bool nomValide = false;
	bool identifiantValide = false;
	bool titreValide = false;
	int annee = 0;
	string nomAuteurs;
	string codeIdentifiant;
	string titre;
	string reponseQuestion;



	while(nomValide == false){
		cout << "Entrez le nom de l'auteur ou des auteurs de la référence" << endl;
		getline(cin, nomAuteurs);

		if (util::validerFormatNom(nomAuteurs)){
			nomValide = true;
			cout << "Le(s) nom(s) est(sont) valide(s)!" << endl;
		}
	}

	while(identifiantValide == false){
		cout << "Entrez l'identifiant de la référence" << endl;
		getline(cin, codeIdentifiant);
		if (util::validerCodeIssn(codeIdentifiant) || util::validerCodeIsbn(codeIdentifiant) ){
			identifiantValide = true;
			cout << "L'identifiant entré est valide!" << endl;
		}
	}

	while (annee <= 0){
		cout << "Entrez l'année d'édition de la référence" << endl;
		cin >> annee;
	}

	while (titreValide == false){
		cout << "Entrez le titre de la référence" << endl;
		cin.ignore();
		getline(cin, titre);
		if (util::validerFormatNom(titre)){
			titreValide = true;
			cout << "Le titre entré est valide!" << endl;
		}
	}

	biblio::Reference referenceUn(nomAuteurs, codeIdentifiant, annee, titre);
	cout << "Les données relatives de la référence sont: " << referenceUn.reqReferenceFormate() << endl;

	/*
	biblio::Reference referenceDeux(nomAuteurs, codeIdentifiant, annee, titre);
	cout << "Les données relatives de la référence deux sont: " << referenceDeux.reqReferenceFormate() << endl;
	 */

	cout << "Voulez-vous modifier les auteurs de la référence (repondre 'oui' ou 'non')" << endl;
	getline(cin, reponseQuestion);
	if (reponseQuestion == "oui"){
		nomValide = false;
		while(nomValide == false) {
			cout << "Entrez de l'auteur ou des auteurs de la référence" << endl;
			getline(cin, nomAuteurs);

			if (util::validerFormatNom(nomAuteurs)){
				nomValide = true;
				cout << "Le(s) nom(s) est(sont) valide(s)!" << endl;
			}
		}
		referenceUn.asgAuteurs(nomAuteurs);
		cout << "La référence est maintenant: " << referenceUn.reqReferenceFormate() << endl;
	}

/*
	string isTheSame;
	cout << "Voulez-vous vérifier si les deux références sont identiques? ( 'oui' ou 'non')" << endl;
	getline (cin, isTheSame);
	if (isTheSame == "oui") {
		bool referenceIdentique = (referenceUn == referenceDeux);
		if (referenceIdentique){
			cout << "Les references sont identiques" << endl;
		}
		else {
			cout << "Les références ne sont pas identiques" << endl;
		}
	}
*/

	cout << "Programme terminé" << endl;
	return 0;
}
