/**
 * \file Reference.cpp
 * \brief Implementation des methodes de la classe Reference
 * \author Toma Gagne
 * \version 1
 * \date 22 fevrier 2019
 */

#include "Reference.h"
#include "validationFormat.h"

using namespace std;

namespace biblio
{

Reference::Reference(string p_auteurs, string p_identifiant, int p_annee, string p_titre)
{
		m_auteurs = p_auteurs;
		m_identifiant = p_identifiant;
		m_annee = p_annee;
		m_titre = p_titre;
}

/**
 * \fn Reference::reqAnnee()
 * \brief Methode d'acces en lecture à l'attribut m_annee
 *
 * \return m_annee
 */
int Reference::reqAnnee() const
{
	return m_annee;
}

/**
 * \fn Reference::reqAuteurs()
 * \brief Methode d'acces en lecture à l'attribut m_auteurs
 *
 * \return m_auteurs
 */
string Reference::reqAuteurs() const
{
	return m_auteurs;
}

/**
 * \fn Reference::reqIdentifiant()
 * \brief Methode d'acces en lecture à l'attribut m_identifiant
 *
 * \return m_identifiant
 */
string Reference::reqIdentifiant() const
{
	return m_identifiant;
}

/**
 * \fn Reference::reqTitre()
 * \brief Methode d'acces en lecture à l'attribut m_titre
 *
 * \return m_titre
 */
string Reference::reqTitre() const
{
	return m_titre;
}

/**
 * \fn Reference::reqReferenceFormate()
 * \brief Methode permettant de formater la reference selon ses attributs
 *
 * \return referenceFormatee, qui est la chaine de caracteres de la reference formatee
 */
string Reference::reqReferenceFormate()
{
	ostringstream s;
	s << m_auteurs << ", " << m_titre << ", " << m_annee << ", " << m_identifiant << ".";
	string referenceFormatee = s.str();
	return referenceFormatee;
}

/**
 * \fn Reference::asgAuteurs()
 * \brief Methode peermettant de modifier les auteurs d'une reference
 * \param[in] p_auteur est le nom de l'auteur ou des auteurs
 * \pre p_auteurs ne doit pas etre nul et doit avoir un format valide
 * \post l'objet courant a été correctement assigne
 * \return m_auteurs
 */
void Reference::asgAuteurs(const string& p_auteurs)
{
	if (util::validerFormatNom(p_auteurs))
	m_auteurs = p_auteurs;
}

/**
 * \fn Reference::operator==()
 * \brief Surcharge de l'operateur "==" pour comparer deux objets Reference
 * \return un booleen
 */
bool Reference::operator==(Reference autreReference) const
{
	bool referenceIdentique = true;
	if (m_auteurs != autreReference.m_auteurs) {referenceIdentique = false;}
	else if (m_identifiant != autreReference.m_identifiant) {referenceIdentique = false;}
	else if (m_annee != autreReference.m_annee) {referenceIdentique = false;}
	else if (m_titre != autreReference.m_titre) {referenceIdentique = false;}
	return referenceIdentique;
}

}

