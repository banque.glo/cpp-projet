#include "validationFormat.h"

bool validerFormatNom(const string& p_nom)
{
	int longueurNom = p_nom.length();
	bool validationNom = true;
	{
		for (int i = 0; i < longueurNom; i++)
		{

			if (p_nom.at(i) == ' ')
			{
				continue;
			}
			else if (!(isalpha(p_nom.at(i))))
			{
				validationNom = false;
				break;
			}
		}
	}
	return validationNom;
}


bool validerCodeIssn(const string& p_issn)
{
	bool validationCode = true;

	istringstream is_chaine (p_issn);


	string acronyme;
	is_chaine >> acronyme;
	if (acronyme != "ISSN")
	{
		validationCode = false;
	}

	string groupeChiffre;
	is_chaine >> groupeChiffre;
	int longueur = groupeChiffre.length();

	if (longueur != 9)
	{
		validationCode = false;
	}

	string chiffreSansTiret = "";
	if (validationCode)
	{
		for (int i = 0; i < longueur; i++)
		{
			if (groupeChiffre.at(i) == '-')
			{
				if (i == 4)
				{
					continue;
				}
				else
				{
					validationCode = false;
					break;
				}
			}

			else if (!(0x30 <= groupeChiffre.at(i)) || !(groupeChiffre.at(i) <= 0x39))
			{
				validationCode = false;
				break;
			}
			else
			{
				chiffreSansTiret += groupeChiffre.at(i);
			}
		}
	}

	if (validationCode)
	{
		if (!(verifierCleIssn(chiffreSansTiret)))
		{
			validationCode = false;
		}
	}
	return validationCode;
}



bool validerCodeIsbn(const string& p_isbn)
{
	bool validation = true;
	istringstream is_chaine(p_isbn);

	string acronyme;
	is_chaine >> acronyme;
	if (acronyme != "ISBN")
	{
		return false;
	}

	string groupeChiffre;
	is_chaine >> groupeChiffre;
	int longueur = groupeChiffre.length();

	if (longueur == 13)
	{
		if (!(validerFormatIsbn10(groupeChiffre, longueur)))
		{
			validation = false;
		}
	}

	else if (longueur == 17)
	{
		if (!(validerFormatIsbn13(groupeChiffre, longueur)))
		{
			validation = false;
		}
	}

	else
	{
		validation = false;
	}

	return validation;
}


bool validerFormatIsbn10(const string& p_isbn10, int longueur)
{
	bool validation = true;
	int nombreTiret = 0;
	string chiffreSansTiret = "";

	for (int i = 0; i < longueur; i++)
	{
		if (p_isbn10.at(i) == '-')
		{
			nombreTiret++;
		}

		else if ((0x30 > p_isbn10.at(i) || p_isbn10.at(i) > 0x39) && p_isbn10.at(i) != 'X')
		{
			validation = false;
			break;
		}
		else
		{
			chiffreSansTiret += p_isbn10.at(i);
		}
	}

	if (nombreTiret != 3)
	{
		validation = false;
	}

	if (validation)
	{
		if (!(verifierCleIsbn10(chiffreSansTiret)))
		{
			validation = false;
		}
	}
	return validation;
}

bool validerFormatIsbn13(const string& p_isbn13, int longueur)
{
	bool validation = true;
	int nombreTiret = 0;
	string chiffreSansTiret = "";

	for (int i = 0; i < longueur; i++)
	{
		if (p_isbn13.at(i) == '-')
		{
			if (i == 3 || i == 5 || i == 10 || i == 15)
			{
				nombreTiret++;
			}
			else
			{
				validation = false;
				break;
			}
		}

		else if (0x30 > p_isbn13.at(i) || p_isbn13.at(i) > 0x39)
		{
			validation = false;
			break;
		}

		else
		{
			chiffreSansTiret += p_isbn13.at(i);
		}
	}

	if (nombreTiret != 4)
	{
		validation = false;
	}

	if (validation)
	{
		if (!(verifierCleIsbn13(chiffreSansTiret)))
		{
			validation = false;
		}
	}
	return validation;
}



bool verifierCleIssn(const string& chiffreSansTiret)
{
	bool validationCle = true;
	int somme = 0;

	for (int j = 8; j >= 2; j--)
	{
		somme += (chiffreSansTiret.at(8-j)- '0') * j;
	}

	int dernierChiffre = chiffreSansTiret.at(7) - '0';
	int resultatCle = 11 - (somme % 11);
	if (dernierChiffre != resultatCle)
	{
		validationCle = false;
	}
	return validationCle;
}

bool verifierCleIsbn10(const string& p_isbn10)
{
	bool validationCle = true;
	char dernierChiffre = p_isbn10.at(9);
	int somme = 0;

	if ((dernierChiffre < '0' || dernierChiffre > '9') && (dernierChiffre != 'X'))
	{
		validationCle = false;
	}

	if (validationCle)
	{
		for (int i = 10; i >= 2; i--)
		{
			somme += ((p_isbn10.at(10 - i) - '0') * i);
		}

		int resultat = somme % 11;

		if (dernierChiffre == 'X')
		{
			if ((resultat + 10) % 11 !=0)
			{
				validationCle = false;
			}

		}

		else
		{
			if ((resultat + (dernierChiffre - '0')) % 11 != 0)
			{
				validationCle = false;
			}
		}
	}

	return validationCle;
}

bool verifierCleIsbn13(const string& p_isbn13)
{
	bool validationCle = true;
	char dernierChiffre = p_isbn13.at(12);
	int somme = 0;

	if ((dernierChiffre < '0' || dernierChiffre > '9'))
	{
		validationCle = false;
	}

	if (validationCle)
	{
		for (int i = 0; i < 12; i++)
		{
			if (i % 2 == 0)
			{
				somme += p_isbn13.at(i) - '0';
			}
			else
			{
				somme += (p_isbn13.at(i) - '0') * 3;
			}
		}

		int resultat = somme % 10;
		int cleRes = 10 - resultat;
		int cle = dernierChiffre - '0';

		if (resultat == 0)
		{
			if (cle != 0)
			{
				validationCle = false;
			}
		}

		else if (resultat != 0)
		{
			if (cle != cleRes)
			{
				validationCle = false;
			}
		}
	}
	return validationCle;
}
