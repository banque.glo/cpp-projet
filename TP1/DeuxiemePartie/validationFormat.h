/*
 * validationFormat.h
 *
 *  Created on: 2019-02-09
 *      Author: etudiant
 */

#ifndef VALIDATIONFORMAT_H_
#define VALIDATIONFORMAT_H_

#include "ctype.h"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

bool validerFormatNom(const string& p_nom);
bool validerCodeIssn(const string& p_issn);
bool validerCodeIsbn(const string& p_isbn);

bool validerFormatIsbn10(const string& p_isbn10, int longueur);
bool validerFormatIsbn13(const string& p_isbn13, int longueur);

bool verifierCleIssn(const string& p_issn);
bool verifierCleIsbn10(const string& p_isbn10);
bool verifierCleIsbn13(const string& p_isbn13);


#endif /* VALIDATIONFORMAT_H_ */
