#include "validationFormat.h"

int main()
{
	string non("Pas correct");
	string oui("Correct");
	string nomFonction;
	cout << "Entrez la fonction à tester" << endl;
	getline(cin, nomFonction);

	if (nomFonction == "validerFormatNom")
	{
		string nom;
		cout << "Entrez le nom" << endl;
		getline(cin, nom);

		if (!(validerFormatNom(nom)))
			{
				cout << non << endl;
			}

			else if (validerFormatNom(nom))
			{
				cout << oui << endl;
			}
	}

	else if (nomFonction == "validerCodeIssn")
	{
		string issn;
		cout << "Entrez le code ISSN" << endl;
		getline(cin, issn);

		if (!(validerCodeIssn(issn)))
		{
			cout << non << endl;
		}
		else if (validerCodeIssn(issn))
		{
			cout << oui << endl;
		}
	}

	else if (nomFonction == "validerCodeIsbn")
	{
		string isbn;
		cout << "Entrez le code ISBN" << endl;
		getline(cin, isbn);

		if (!(validerCodeIsbn(isbn)))
		{
			cout << non << endl;
		}

		else if (validerCodeIsbn(isbn))
		{
			cout << oui << endl;
		}
	}
	return 0;
}
