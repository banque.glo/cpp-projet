/*
 * fonctionsUtilitaires.h
 *
 *  Created on: 2019-01-10
 *      Author: theud1
 *      A corriger
 */

#ifndef FONCTIONSUTILITAIRES_H_
#define FONCTIONSUTILITAIRES_H_

#include <iostream>

#define MAX_ETUDIANTS 4

static const int MAX_EXAMENS = 2;

void saisieNotes(int tabNotes[][MAX_EXAMENS], int*);
int minimum(int tabNotes[][MAX_EXAMENS], int*);
int maximum(int tabNotes[][MAX_EXAMENS], int*);
double ecartType (int tabNotesEtudiant[MAX_EXAMENS]);
void afficherTableau(int tabNotes[][MAX_EXAMENS], int*);

#endif /* FONCTIONSUTILITAIRES_H_ */
